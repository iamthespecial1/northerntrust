import React from 'react';
import logo from './logo.svg';
import './App.css';
import PokeView from "./PokeViewer/index"
function App() {
  return (
    <div>
      <PokeView />
    </div>
  );
}

export default App;
