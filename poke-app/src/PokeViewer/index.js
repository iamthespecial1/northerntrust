import React from 'react';
import Viewer from "./pokeview"
import "./pokemon.css"
class PokeView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentThree: [],
            curr: 0,
        }
    }
    findPoke = (url) => {
        return fetch(`https://pokeapi.co/api/v2/pokemon/${url}`).then(res => res.json()).then(val => {
            return {
                name: val.name,
                front_image: val.sprites.front_default,
                id: val.id
            }
        })
    }

    fetchPoke = (url) => {
        fetch(url)
            .then(res => res.json())
            .then(
                (val) => {
                    // console.log(val)
                    this.setState({
                        data: val.results,
                        nextUrl: val.next,
                        prevUrl: val.previous,
                        count: 3,
                        curr: 0
                    });
                    let promise1 = this.findPoke(val.results[0].url)
                    let promise2 = this.findPoke(val.results[1].url)
                    let promise3 = this.findPoke(val.results[2].url)
                    Promise.all([promise1, promise2, promise3]).then(res => {
                        this.setState({
                            currentThree: res
                        })
                    })
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    componentDidMount() {
        this.handleNext()
    }

    handleNext = () => {
        const { curr } = this.state
        let promise1 = this.findPoke(curr * 3 + 1)
        let promise2 = this.findPoke(curr * 3 + 2)
        let promise3 = this.findPoke(curr * 3 + 3)
        Promise.all([promise1, promise2, promise3]).then(res => {
            this.setState((prevState) => ({
                curr: prevState.curr + 1,
                currentThree: res
            }))
        })

    }

    handlePrevious = () => {
        const { curr } = this.state
        let promise1 = this.findPoke(curr * 3 + 1)
        let promise2 = this.findPoke(curr * 3 + 2)
        let promise3 = this.findPoke(curr * 3 + 3)
        Promise.all([promise1, promise2, promise3]).then(res => {
            this.setState((prevState) => ({
                curr: prevState.curr - 1,
                currentThree: res
            }))
        })
    }

    render() {
        console.log(this.state.curr)
        return <>
            <div className="d-flex paddingL-8">
                {
                    this.state.currentThree.map(val => <Viewer key={val.id} data={val} />)
                }
            </div>
            <button disabled={this.state.curr === 1 ? 1 : 0} onClick={this.handlePrevious}>Previous</button>
            <button className="floatr" onClick={this.handleNext}>Next</button>
        </>;
    }
}

export default PokeView;
