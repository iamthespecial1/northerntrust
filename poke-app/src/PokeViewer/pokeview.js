import React from 'react';
import "./pokemon.css"
function Viewer(props) {
    return (
        <div className="box">
            <div className="d-flex">
                {props.data.name}  ID:{props.data.id}
            </div>
            <img
                src={props.data.front_image}
                alt={props.data.name}
            />
        </div>
    );
}

export default Viewer;